package com.template.tree;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class AVLTreeTest {
    private AVLTree tree;

    @Before
    public void setUp() {
        tree = new AVLTree();
    }

    @Test
    public void testEmptyTree() {
        assertNull(tree.search(1));
        assertFalse(tree.contains(1));
        assertEquals("", tree.inOrder());
    }

    @Test
    public void testInsert() {
        tree.insert(4);
        tree.insert(2);
        tree.insert(6);
        tree.insert(1);
        tree.insert(3);
        tree.insert(5);
        tree.insert(7);

        assertTrue(tree.contains(1));
        assertTrue(tree.contains(2));
        assertTrue(tree.contains(3));
        assertTrue(tree.contains(4));
        assertTrue(tree.contains(5));
        assertTrue(tree.contains(6));
        assertTrue(tree.contains(7));

        assertEquals("1 2 3 4 5 6 7", tree.inOrder());
    }

    @Test
    public void testDeleteLeafNode() {
        tree.insert(4);
        tree.insert(2);
        tree.insert(6);
        tree.insert(1);
        tree.insert(3);
        tree.insert(5);
        tree.insert(7);

        tree.delete(1);

        assertFalse(tree.contains(1));
        assertEquals("2 3 4 5 6 7", tree.inOrder());
    }

    @Test
    public void testDeleteNodeWithOneChild() {
        tree.insert(4);
        tree.insert(2);
        tree.insert(6);
        tree.insert(1);
        tree.insert(3);
        tree.insert(5);
        tree.insert(7);

        tree.delete(3);

        assertFalse(tree.contains(3));
        assertEquals("1 2 4 5 6 7", tree.inOrder());
    }

    @Test
    public void testDeleteNodeWithTwoChildren() {
        tree.insert(4);
        tree.insert(2);
        tree.insert(6);
        tree.insert(1);
        tree.insert(3);
        tree.insert(5);
        tree.insert(7);

        tree.delete(2);

        assertFalse(tree.contains(2));
        assertEquals("1 3 4 5 6 7", tree.inOrder());
    }
}
