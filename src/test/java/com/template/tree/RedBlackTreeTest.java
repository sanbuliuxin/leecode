package com.template.tree;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author shenb
 * @date 2023-03-28 18:26
 */
public class RedBlackTreeTest {


    @Test
    public void testInsert() {
        RedBlackTree<Integer> tree = new RedBlackTree<>();
        tree.insert(1);
        tree.insert(2);
        tree.insert(4);
        tree.insert(3);
        tree.insert(6);
        tree.insert(5);
        tree.insert(7);

        tree.printTree();
    }
}