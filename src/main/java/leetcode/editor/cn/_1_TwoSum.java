//给定一个整数数组 nums 和一个整数目标值 target，请你在该数组中找出 和为目标值 target 的那 两个 整数，并返回它们的数组下标。 
//
// 你可以假设每种输入只会对应一个答案。但是，数组中同一个元素在答案里不能重复出现。 
//
// 你可以按任意顺序返回答案。 
//
// 
//
// 示例 1： 
//
// 
//输入：nums = [2,7,11,15], target = 9
//输出：[0,1]
//解释：因为 nums[0] + nums[1] == 9 ，返回 [0, 1] 。
// 
//
// 示例 2： 
//
// 
//输入：nums = [3,2,4], target = 6
//输出：[1,2]
// 
//
// 示例 3： 
//
// 
//输入：nums = [3,3], target = 6
//输出：[0,1]
// 
//
// 
//
// 提示： 
//
// 
// 2 <= nums.length <= 10⁴ 
// -10⁹ <= nums[i] <= 10⁹ 
// -10⁹ <= target <= 10⁹ 
// 只会存在一个有效答案 
// 
//
// 进阶：你可以想出一个时间复杂度小于 O(n²) 的算法吗？ 
//
// 👍 15757, 👎 0bug 反馈 | 使用指南 | 更多配套插件 
//
//
//
//


package leetcode.editor.cn;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class _1_TwoSum {

  public static void main(String[] args) {
    Solution solution = new _1_TwoSum().new Solution();
    solution.twoSum(new int[]{3, 2, 4}, 6);
  }

  //leetcode submit region begin(Prohibit modification and deletion)
  class Solution {

    public int[] twoSum(int[] nums, int target) {
      Map<Integer, List<Integer>> numToIndex = new HashMap<>();
      for (int i = 0; i < nums.length; i++) {
        if (!numToIndex.containsKey(nums[i])) {
          numToIndex.put(nums[i], new ArrayList<>());
        }
        List<Integer> indexes = numToIndex.get(nums[i]);
        indexes.add(i);
      }
      for (int num : nums) {
        int other = target - num;
        if (numToIndex.containsKey(other)) {
          if (num == other) {
            if (numToIndex.get(num).size() > 1) {
              return new int[]{numToIndex.get(num).get(0), numToIndex.get(other).get(1)};
            }
          } else {
            return new int[]{numToIndex.get(num).get(0), numToIndex.get(other).get(0)};
          }
        }
      }
      return new int[0];
    }
  }
//leetcode submit region end(Prohibit modification and deletion)

}