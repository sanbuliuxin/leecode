package leetcode.editor.cn.utils;

/**
 * @author shenb
 * @date 2023-01-06 14:45
 */
public class ListNode {
    public  int val;
    public ListNode next;

    public ListNode() {
    }

    public ListNode(int val) {
        this.val = val;
    }

    public  ListNode(int val, ListNode next) {
        this.val = val;
        this.next = next;
    }

    public  ListNode(int[] arr) {
        if (arr == null || arr.length == 0) {
            return;
        }
        this.val = arr[0];
        ListNode n = this;
        for (int i = 1; i < arr.length; i++) {
            n.next = new ListNode(arr[i]);
            n = n.next;
        }
    }

    @Override
    public String toString() {
        ListNode n = this;
        StringBuilder sb = new StringBuilder();
        while (n != null) {
            sb.append(n.val).append(" -> ");
            n = n.next;
        }
        return sb.toString();
    }
}
