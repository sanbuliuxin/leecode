//给你一个链表的头节点 head 和一个特定值 x ，请你对链表进行分隔，使得所有 小于 x 的节点都出现在 大于或等于 x 的节点之前。 
//
// 你应当 保留 两个分区中每个节点的初始相对位置。 
//
// 
//
// 示例 1： 
// 
// 
//输入：head = [1,4,3,2,5,2], x = 3
//输出：[1,2,2,4,3,5]
// 
//
// 示例 2： 
//
// 
//输入：head = [2,1], x = 2
//输出：[1,2]
// 
//
// 
//
// 提示： 
//
// 
// 链表中节点的数目在范围 [0, 200] 内 
// -100 <= Node.val <= 100 
// -200 <= x <= 200 
// 
//
// 👍 666, 👎 0bug 反馈 | 使用指南 | 更多配套插件 
//
//
//
//

  
package leetcode.editor.cn;
import leetcode.editor.cn.utils.ListNode;
import leetcode.editor.cn.utils.Utils;

import java.util.*;

public class _86_PartitionList{
  public static void main(String[] args) {
       Solution solution = new _86_PartitionList().new Solution();
      ListNode partition = solution.partition(new ListNode(new int[]{1, 4, 3, 2, 5, 2}), 3);
      System.out.println(partition);
  }
  
  //leetcode submit region begin(Prohibit modification and deletion)


class Solution {
    public ListNode partition(ListNode head, int x) {

        ListNode l1 = new ListNode(-1), l2 =new ListNode(-1);
        ListNode dummy = l1, dummy2 = l2;
        while (head != null) {
            if (head.val < x) {
                l1.next = head;
                l1 = head;
            }else if(head.val >= x){
                l2.next = head;
                l2 = head;
            }
            head = head.next;
        }
        l2.next = null;
        l1.next = dummy2.next;
        return dummy.next;

    }
}
//leetcode submit region end(Prohibit modification and deletion)

}