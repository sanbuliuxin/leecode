//给你一个链表，删除链表的倒数第 n 个结点，并且返回链表的头结点。 
//
// 
//
// 示例 1： 
// 
// 
//输入：head = [1,2,3,4,5], n = 2
//输出：[1,2,3,5]
// 
//
// 示例 2： 
//
// 
//输入：head = [1], n = 1
//输出：[]
// 
//
// 示例 3： 
//
// 
//输入：head = [1,2], n = 1
//输出：[1]
// 
//
// 
//
// 提示： 
//
// 
// 链表中结点的数目为 sz 
// 1 <= sz <= 30 
// 0 <= Node.val <= 100 
// 1 <= n <= sz 
// 
//
// 
//
// 进阶：你能尝试使用一趟扫描实现吗？ 
//
// 👍 2353, 👎 0bug 反馈 | 使用指南 | 更多配套插件 
//
//
//
//

  
package leetcode.editor.cn;
import leetcode.editor.cn.utils.ListNode;

import java.util.*;

public class _19_RemoveNthNodeFromEndOfList{
  public static void main(String[] args) {
       Solution solution = new _19_RemoveNthNodeFromEndOfList().new Solution();
      solution.removeNthFromEnd(new ListNode(new int[]{1, 2, 3, 4, 5}), 2);
  }
  
  //leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode() {}
 *     ListNode(int val) { this.val = val; }
 *     ListNode(int val, ListNode next) { this.val = val; this.next = next; }
 * }
 */

class Solution {
    public ListNode removeNthFromEnd(ListNode head, int n) {
        /**
         * 不过注意我们又使用了虚拟头结点的技巧，也是为了防止出现空指针的情况
         * 比如说链表总共有 5 个节点，题目就让你删除倒数第 5 个节点，
         * 也就是第一个节点，那按照算法逻辑，应该首先找到倒数第 6 个节点。
         * 但第一个节点前面已经没有节点了，这就会出错。
         */
        ListNode dummy =new ListNode(-1);
        dummy.next = head;

        ListNode p = dummy;
        for (int i = 0; i < n + 1; i++) {
            p = p.next;
            if (p == null && i != n) {
                return dummy.next;
            }
        }
        ListNode r = dummy;
        while (p != null) {
            p = p.next;
            r = r.next;
        }
        r.next = r.next.next;
        return dummy.next;

    }
}
//leetcode submit region end(Prohibit modification and deletion)

}