package com.template;

import java.util.Arrays;

/**
 * @author shenb
 * @date 2022-02-22 22:14
 */
public class Random {

  public static final java.util.Random random = new java.util.Random();

  /**
   * 返回[smallest, biggest)范围的数组
   *
   * @param size 数组大小
   * @param smallest  最小值
   * @param biggest   最大值
   * 
   */
  public static int[] randomInts(int size, int smallest, int biggest) {
    int[] ints = new int[size];
    int i1 = biggest - smallest;
    for (int i = 0; i < size; i++) {
      ints[i] = smallest + random.nextInt(i1);
    }
    return ints;
  }

  /**
   * 返回[0, 10)范围的数组
   *
   * @param size 数组大小
   * 
   */
  public static int[] randomInts(int size) {
    return randomInts(size, 0, 10);
  }

  /**
   * 返回[0, 10)范围的数组，数组长度在0-9之间随机
   *
   * 
   */
  public static int[] randomInts() {
    int size = random.nextInt(10);
    return randomInts(size);
  }

  /**
   * 返回[0, 10)范围的排序数组，数组长度在0-9之间随机
   * 
   */
  public static int[] randomSortInts() {
    int[] ints = randomInts();
    Arrays.sort(ints);
    return ints;
  }

  /**
   * 返回一个随机二叉树，树的最大深度是3
   */
  public static TreeNode randomTree() {
    int depth = 1;
    TreeNode node = new TreeNode(random.nextInt(10));
    node.left = randomExpand(1);
    node.right = randomExpand(1);
    return node;
  }

  /**
   * 随机创建一个树节点，包括子节点
   * @param currentDepth  当前深度，如果当前深度等于3，则不继续创建子节点
   * @return  树节点
   */
  private static TreeNode randomExpand(int currentDepth) {
    if (currentDepth == 3) {
      return null;
    }
    int left = random.nextInt(9);
    if (left == 0) {
      return null;
    } else {
      int depth = currentDepth +1;
      return new TreeNode(left, randomExpand(depth), randomExpand(depth));
    }
  }

}
