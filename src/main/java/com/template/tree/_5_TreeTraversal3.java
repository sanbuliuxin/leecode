package com.template.tree;

import com.template.TreeNode;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * @author shenb
 * @date 2022-02-26 16:40
 */
public class _5_TreeTraversal3 {

  /**
   * 层序遍历
   */
  public static List<Integer> levelOrder(TreeNode root) {
    List<Integer> list = new ArrayList<>();

    Queue<TreeNode> queue = new LinkedList<>();
    queue.add(root);

    while (!queue.isEmpty()) {
      TreeNode poll = queue.poll();
      list.add(poll.val);
      if (poll.left != null) {
        queue.offer(poll.left);
      }
      if (poll.right != null) {
        queue.offer(poll.right);
      }
    }
    return list;
  }

  public static void main(String[] args) {

  }
}
